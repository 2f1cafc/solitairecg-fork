/*
 * generates random numbers according to the MS Freecell algorithm, as
 * per the details here:
 * https://rosettacode.org/wiki/Deal_cards_for_FreeCell
 */

package net.sourceforge.solitaire_cg;

import java.util.Random;

public class MSRand {

  final static int mask = (1 << 31) - 1;

  private int seed;

  public MSRand(int seed) {
	this.seed = seed;
  }

  public int nextInt() {
	seed = (seed * 214013 + 2531011) & mask;
	return seed >> 16;
  }

  public int nextInt(int max) {
	return nextInt() % max;
  }

  public static MSRand randomSeed() {
	return new MSRand(new Random().nextInt(32000) + 1); // randomly selects one of the MS numbered games 1..32000
  }

}
