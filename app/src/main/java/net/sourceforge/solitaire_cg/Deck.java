/*
  Copyright 2008 Google Inc.

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.

  Modified by Curtis Gedak 2015
*/
package net.sourceforge.solitaire_cg;

public class Deck {

  private Card[] mCard;
  private int mCardCount;
  private MSRand rand;

  public Deck(int decks) {
	this(decks, MSRand.randomSeed());
  }

  public Deck(int decks, MSRand rand) {
	this.rand = rand;
    Init(decks, 4);
  }

  public Deck(int decks, int suits) {
	this(decks, suits, MSRand.randomSeed());
  }

  public Deck(int decks, int suits, MSRand rand) {
    if (suits == 2) {
      decks *= 2;
    } else if (suits == 1) {
      decks *= 4;
    }
	this.rand = rand;
    Init(decks, suits);
  }

  private void ConfigureDeck(int decks, int suits) {
    mCardCount = decks * 13 * suits;
    mCard = new Card[mCardCount];
    for (int deck = 0; deck < decks; deck++) {
	  for (int value = 0; value < 13; value++) {
		for (int suit = Card.CLUBS; suit < suits; suit++) {
          mCard[deck*suits*13 + value*suits + suit] = new Card(value+1, Card.SUIT_ORDER[suit]);
        }
      }
    }
  }

  private void Init(int decks, int suits) {
	ConfigureDeck(decks, suits);
	Shuffle();
    // Shuffle();
    // Shuffle();
  }

  public void PushCard(Card card) {
    mCard[mCardCount++] = card;
  }

  public Card PopCard() {
    if (mCardCount > 0) {
      return mCard[--mCardCount];
    }
    return null;
  }

  public boolean Empty() {
    return mCardCount == 0;
  }

  public void Shuffle() {
    int lastIdx = mCardCount - 1;
    int swapIdx;
    Card swapCard;
	// MSRand rand = new MSRand(1);
	// Random rand = new Random();

    while (lastIdx > 1) {
      swapIdx = rand.nextInt(lastIdx + 1);
      swapCard = mCard[swapIdx];
      mCard[swapIdx] = mCard[lastIdx];
      mCard[lastIdx] = swapCard;
      lastIdx--;
    }
  }
}
